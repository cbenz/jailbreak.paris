<!--
.. title: La taskforce
.. slug: taskforce
.. date: 2017-03-26 01:21:39 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

jailbreak.paris est une **taskforce pluridisciplinaire**. Voici nos compétences :

- Développement
- Administration de systèmes
- Data science
- Cartographie
- Design
- Animation de communauté
- Organisation d'événements (hackathons, open data camps, mapathons...)
- Gestion de produit
- Gestion de projet
- Stratégie et communication
- Relations institutionnelles

L'équipe est constituée de profils expérimentés, spécialisés dans le **logiciel libre**, l'**open data**, le **gouvernement ouvert** et les **méthodes agiles**.

Ses membres sont profondément insérés dans les communautés du "mouvement open" en France et à l'international ([Open Government Partnership](http://www.opengovpartnership.org/), [April](http://www.april.org/), [OpenStreetMap](http://www.openstreetmap.org/), [Open Democracy Now](http://opendemocracynow.net/), [Code for Europe](http://codeforeurope.net/), [CartONG](www.cartong.org/)...) et ont mené des projets d'ouverture par le numérique éprouvés ([Réseau Libre-Entreprise](http://www.libre-entreprise.org/), [BA evoluciona](http://www.buenosaires.gob.ar/evoluciona), [data.gouv.fr](https://www.data.gouv.fr/), [OpenFisca](https://www.openfisca.fr/), [OGP Toolbox](http://ogptoolbox.org/), [Sommet OGP 2016](https://ogpsummit.org)...).
