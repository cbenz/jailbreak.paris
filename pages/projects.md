<!--
.. title: Current Projects
.. slug: projects
.. date: 2017-03-26 01:21:39 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

---

## DBnomics

[DBnomics](https://db.nomics.world/) is an **open data portal** aggregating public economic data as released by national and international producers (national institutes of statistics, central banks, IMF, World Bank, OECD, Eurostat, ECB).

The website is intended for researchers working on reproducible macroeconomics, public policy experts, data-journalists, specialized civil society organisation, civil servants and decision makers.

DBnomics is developed by jailbreak.paris within the [Center for economic research and its applications](http://www.cepremap.fr/) (CEPREMAP).

To follow up the project: [join the forum](https://forum.jailbreak.paris/t/dbnomics/68).

---

## Retruco

[Retruco](https://framagit.org/retruco/retruco-api) is a software fostering argumentative discussion around statements and allowing to bring out shared positions.

Retruco means immediate, precise and firm response in Spanish.

The [OGP Toolbox](http://www.ogptoolbox.org/) is powered by the Retruco engine.

To follow up the project: [join the forum](https://forum.jailbreak.paris/t/retruco/69).

---

## Open government consulting

Expert advice and guidance for international funding organizations:

- Mapping of the current state of e-gov and open gov actors and initiatives in France

- Trend analysis (opportunities and challenges) of the "open movement" in France
