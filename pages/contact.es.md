<!--
.. title: Contacto
.. slug: contact
.. date: 2017-03-26 01:36:40 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Para contactar el equipo : [contact@jailbreak.paris](mailto:contact@jailbreak.paris).

Para dialogar con la comunidad de jailbreak.paris : [bienvenido a nuestro foro](https://forum.jailbreak.paris/).

Síganos en [Twitter](https://twitter.com/jailbreak_paris).