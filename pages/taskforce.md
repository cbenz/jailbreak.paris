<!--
.. title: Taskforce
.. slug: taskforce
.. date: 2017-03-26 01:21:39 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

jailbreak.paris is a **multidisciplinary taskforce**. Here's an overview of our key strengths:

- Software development
- System administration
- Data science
- Cartography
- Design
- Community animation
- Event planning (hackathons, open data camps, mapathons...)
- Product management
- Project management
- Strategy and communication
- Institutional relations

Our team is made of experimented individuals, specialized in **open source**, **open data**, **open government** and **agile development**.

Team members are deeply involved in the "open movement" communities in France and abroad ([Open Government Partnership](http://www.opengovpartnership.org/), [April](http://www.april.org/), [OpenStreetMap](http://www.openstreetmap.org/), [Open Democracy Now](http://opendemocracynow.net/), [Code for Europe](http://codeforeurope.net/), [CartONG](www.cartong.org/)...) and have carried out successful digital-driven open strategies ([Réseau Libre-Entreprise](http://www.libre-entreprise.org/), [BA evoluciona](http://www.buenosaires.gob.ar/evoluciona), [data.gouv.fr](https://www.data.gouv.fr/), [OpenFisca](https://www.openfisca.fr/), [OGP Toolbox](http://ogptoolbox.org/), [Sommet OGP 2016](https://ogpsummit.org)...).
