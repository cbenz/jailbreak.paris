<!--
.. title: Notre ADN
.. slug: about
.. date: 2017-03-26 01:07:21 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

- **Stratégies et pratiques d'ouverture** par le numérique
- Projets **de bout en bout**: depuis la conception jusqu'à la mise en oeuvre
- **Pouvoir d'agir** basé sur la capacité à mobiliser les talents là ou ils se trouvent
- Réseau **à la croisée de différents ecosystèmes**: administrations, centres de recherche, entreprises, associations, communautés...
- Impact **international**
- Méthodes **collaboratives et transparentes**


[*Les baselines auxquelles vous avez échappés*](https://forum.jailbreak.paris/t/les-baselines-auxquelles-vous-avez-echappe-the-baselines-from-which-you-have-been-spared/71).
