<!--
.. title: Projets en cours
.. slug: projects
.. date: 2017-03-26 01:21:39 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

---

## DBnomics

[DBnomics](https://db.nomics.world/) est un **portail d'open data** qui recueille des données économiques rendues disponibles par des producteurs nationaux ou internationaux (instituts nationaux de statistiques, banques centrales, FMI, Banque Mondiale, OCDE, Eurostat, BCE).

Le site est destiné aux chercheurs en macroéconomie intéressés par la science reproductible, experts en politiques publiques, data-journalistes, organisations de la société civile spécialisées et aux décideurs.

DBnomics est développé par jailbreak.paris au sein du [Centre pour la recherche économique et ses applications](http://www.cepremap.fr/) (CEPREMAP).

Pour suivre l'avancement du projet, [rejoignez notre forum](https://forum.jailbreak.paris/t/dbnomics/68).

---

## Retruco

[Retruco](https://framagit.org/retruco/retruco-api) est un **logiciel libre favorisant la discussion argumentée** et permettant l'identification de positions partagées de façon automatique.

Retruco (espagnol) fait référence à une réponse immédiate, précise et ferme.

Le moteur Retruco propulse l'[OGP Toolbox](http://www.ogptoolbox.org/).

Pour suivre l'avancement du projet, [rejoignez notre forum](https://forum.jailbreak.paris/t/retruco/69).

---

## Expertise gouvernement ouvert

Conseil et accompagnement pour des bailleurs internationaux reconnus :

- Cartographie des principaux acteurs et projets de l'*e-gov* et du gouvernement ouvert en France

- Identification des tendances (obstacles et opportunités) du mouvement d'ouverture en France
