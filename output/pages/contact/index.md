<!--
.. title: Contact
.. slug: contact
.. date: 2017-03-26 01:36:40 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

To contact the team: [contact@jailbreak.paris](mailto:contact@jailbreak.paris).

To discuss with the jailbreak.paris community: [join the forum](https://forum.jailbreak.paris/).
