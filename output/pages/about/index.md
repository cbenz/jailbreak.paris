<!--
.. title: About
.. slug: about
.. date: 2017-03-26 01:07:21 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

**Jailbreak**
Definition:
*v.* Modify a smartphone or other electronic device to remove restrictions imposed by the manufacturer, e.g. to allow the installation of unauthorized software.
*v.* Escape from prison.
*Syn.* **Deliver.**
*Ex:* Jailbreak: Deliver your digital potential.

- **Digital-driven** open strategies
- **End-to-end** solutions from conception to delivery
- Talent unlocking to **empower teams**
- A network **at the crossroads of different ecosystems**: public administrations, research centers, companies, nonprofits, communities...
- **International** impact
- **Transparent and collaborative** methods

[*The baselines from which you have been spared*](https://forum.jailbreak.paris/t/les-baselines-auxquelles-vous-avez-echappe-the-baselines-from-which-you-have-been-spared/71).
