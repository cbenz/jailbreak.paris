<!--
.. title: Breaking the codes!
.. slug:
.. date: 2017-03-26 01:07:21 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

<div class="jumbotron">
    <div class="container">
        <h1>jailbreak.paris — Breaking the codes!</h1>
        <div class="row">
            <div class="col-md-8">
                <p>In goes content, out comes a website, ready to deploy.</p>
                <p><a class="btn btn-primary btn-lg get-started" href="/getting-started.html" role="button">Get started with Nikola »</a> <a class="btn btn-default btn-lg" href="/features/index.html" role="button">Learn about Nikola features »</a></p>
                <p><small>Nikola is written in Python (3.3+; 2.7 partially supported). Free open-source software under the <a href="/license.html">MIT license</a>.</small></p>
            </div>
            <div class="col-md-4" style="min-height: 230px;">
                <a class="twitter-timeline" data-lang="en" data-height="200" href="https://twitter.com/GetNikola">Tweets by GetNikola</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
            </div>
        </div>
    </div>
</div>
> **Jailbreak**
>
> *verb* Modify a smartphone or other electronic device to remove restrictions imposed by the manufacturer, e.g. to allow the installation of unauthorized software.
>
> *verb* Escape from prison.
>
> *Syn.* **Deliver.**
>
> *Ex:* Jailbreak: Deliver your digital potential.
