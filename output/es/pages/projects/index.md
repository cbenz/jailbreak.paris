<!--
.. title: Los proyectos
.. slug: projects
.. date: 2017-03-26 01:21:39 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Proyectos en curso :

### DBnomics

[DBnomics](https://db.nomics.world/) es un **portal de datos abiertos** que integra datos económicos difundidos por productores nacionales e internacionales (institutos nacionales de estadística, bancos centrales, FMI, Banco Mundial, OECD, Eurostat, BCE).

El sitio está destinado a investigadores interesados en la macroeconomía reproductible, expertos en políticas públicas, periodistas de datos, organizaciones de la sociedad civil especializadas y tomadores de decisiones.

EL desarrollo de DBnomics está impulsado por el [CEPREMAP](http://www.cepremap.fr/).

Para seguir el proyecto, [bienvenido a nuestro foro](https://forum.jailbreak.paris/t/dbnomics/68).


### Retruco

[Retruco](https://framagit.org/retruco/retruco-api) es un programa que promueve la discusión argumentada y permite la identificación automática de posiciones compartidas.

Retruco hace referencia al acto de responder rápidamente, de manera precisa y firme.

El motor Retruco alimenta el portal [OGP Toolbox](http://www.ogptoolbox.org/).

Para seguir el proyecto: [bienvenido a nuestro foro](https://forum.jailbreak.paris/t/retruco/69).

### Consultoría en gobierno abierto

Consultoría y acompañamiento para reconocidos financiadores internacionales :

- Cartografía de los principales actores y proyectos de *e-gov* y de gobierno abierto en Francia

- Identificación de las tendencias (obstáculos y oportunidades) del movimiento de apertura en Francia
