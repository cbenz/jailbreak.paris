<!--
.. title: El equipo
.. slug: taskforce
.. date: 2017-03-26 01:21:39 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Jailbreak.paris es un **equipo pluridisciplario**. Nuestras competencias :

- Desarrollo digital
- Administración de sistemas
- Ciencia de datos
- Cartografía
- Diseño
- Animación de comunidades
- Organización de eventos (hackathones, open data camps, mapathones...)
- Gestión de productos
- Gestión de proyectos
- Estrategia y comunicación
- Relaciones institucionales

El equipo está compuesto por perfiles experimentados, especializados en el **código abierto**, los **datos abiertos**, el **gobierno abierto** y las **metodologías ágiles**.

Sus miembros participan activamente de las comunidades del movimiento "open" en Francia y en el mundo ([Open Government Partnership](http://www.opengovpartnership.org/), [April](http://www.april.org/), [OpenStreetMap](http://www.openstreetmap.org/), [Open Democracy Now](http://opendemocracynow.net/), [Code for Europe](http://codeforeurope.net/), [CartONG](www.cartong.org/)...) y llevaron a cabo proyectos de apertura digital exitosos ([Réseau Libre-Entreprise](http://www.libre-entreprise.org/), [BA evoluciona](http://www.buenosaires.gob.ar/evoluciona), [data.gouv.fr](https://www.data.gouv.fr/), [OpenFisca](https://www.openfisca.fr/), [OGP Toolbox](http://ogptoolbox.org/), [Sommet OGP 2016](https://ogpsummit.org)...).
