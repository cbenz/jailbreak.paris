<!--
.. title: Nosotros
.. slug: about
.. date: 2017-03-26 01:07:21 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

**Jailbreak**
Definición :
*v.* Modificar un teléfono u otro dispositivo electrónico con el fin de quitar las restricciones impuestas por el fabricante para permitir, por ejemplo, la instalación de aplicaciones no autorizadas.
*v.* Escaparse de la cárcel.
*Sin.* **Liberar.**
*Ej.* Jailbreak : Libera tu potencial digital.


- Estrategias y prácticas de **apertura digital**
- **Proyectos integrales**: desde la concepción hasta la implementación
- Identificación y mobilización de talentos para **empoderar equipos**
- Acceso a **una red que cruza diferentes ecosistemas**: administraciones, centros de investigación, empresas, asociaciones, comunidades...
- Impacto **internacional**
- Métodologías **colaborativas y transparentes**

[*Las baselines de las cuales escaparon*](https://forum.jailbreak.paris/t/les-baselines-auxquelles-vous-avez-echappe-the-baselines-from-which-you-have-been-spared/71).
